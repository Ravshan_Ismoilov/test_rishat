FROM python:3.10.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /test

COPY Pipfile Pipfile.lock /test/
RUN pip install pipenv && pipenv install --system

COPY . /test/