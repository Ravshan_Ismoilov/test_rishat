from django.shortcuts import render, redirect, get_object_or_404
from mainapp.models import Item
from cart.cart import Cart
from cart.forms import CartAddProductForm



def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Item, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        if cd['quantity'] == 0:
            cart_remove(request, product_id)
        else:
            cart.add(product=product, quantity=cd['quantity'], update_quantity=cd['update'])
    return redirect('cart:cart_detail')

def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Item, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')

def cart_detail(request):
    cart = Cart(request)
    if cart:
        for item in cart:
            item['update_quantity_form'] = CartAddProductForm(initial={'quantity': item['quantity'],'update': True})
        return render(request, 'cart/detail.html', locals())
    else:
        return redirect('mainapp:item_list')