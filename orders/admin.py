from django.contrib import admin
from orders.models import Order, OrderItem





class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']

class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['order', 'product', 'price', 'quantity']
    list_filter = ['order', 'product', 'price', 'quantity']

class OrderAdmin(admin.ModelAdmin):
    list_display = ['customer', 'paid', 'payment_status', 'created', 'payment_id', 'payment_link',]
    list_filter = ['paid', 'created', 'payment_id', 'payment_link']
    inlines = [OrderItemInline]

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)