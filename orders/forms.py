from django import forms
from orders.models import Order

class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['customer', 'sponsor', 'payment_status', 'paid']

    def __init__(self, *args, **kwargs):
        super(OrderCreateForm, self).__init__(*args, **kwargs)
        self.fields['customer'].widget.attrs.update({'class' : 'form-control'})
        self.fields['sponsor'].widget.attrs.update({'class' : 'form-control'})
        self.fields['payment_status'].widget.attrs.update({'class' : 'form-control'})
        self.fields['paid'].widget.attrs.update({'class' : 'form-control'})


