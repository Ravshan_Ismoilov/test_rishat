from django.db import models
from mainapp.models import Item
from django.contrib.auth import get_user_model
from decimal import Decimal

User = get_user_model()


STATUS_CHOICES = (
		('usd', 'USD'),
		('eur', 'EUR'),
	)

class Order(models.Model):
	customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="related_customer", verbose_name="Customer")
	paid = models.BooleanField(default=False, verbose_name="Payment status")
	created = models.DateTimeField(auto_now_add=True)
	payment_status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='usd', verbose_name="Payment type")
	payment_id = models.CharField(max_length=255, verbose_name="Payment id", blank=True, null=True)
	payment_link = models.CharField(max_length=255, verbose_name="Payment link", blank=True, null=True)

	class Meta:
		ordering = ('-created',)

	def __str__(self):
		return 'Order#{}'.format(self.id)

	def get_total_cost(self):
		return sum(item.get_cost() for item in self.items.all())

class OrderItem(models.Model):
	order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE, verbose_name="Order")
	product = models.ForeignKey(Item, related_name='order_items', on_delete=models.CASCADE, verbose_name="Item")
	price = models.DecimalField(max_digits=15, decimal_places=2, verbose_name="Price")
	quantity = models.PositiveIntegerField(default=1, verbose_name="Quantity")

	def __str__(self):
		return str(self.id)

	def get_cost(self):
		return Decimal(self.price * self.quantity)
		# return int(self.price) * self.quantity

