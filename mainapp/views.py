from django.shortcuts import redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.http import JsonResponse
from django.contrib import messages
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
import stripe

from mainapp.models import Item
from orders.models import Order, OrderItem
from cart.forms import CartAddProductForm
from cart.cart import Cart

stripe.api_key = settings.STRIPE_SECRET_KEY
M_DOMAIN = 'http://localhost:8000'

# @login_required
class ItemListView(LoginRequiredMixin, ListView, FormView):
    login_required = True
    model = Item
    context_object_name = 'items'
    form_class = CartAddProductForm
    template_name = 'item_list.html'

class ItemDetailView(LoginRequiredMixin, DetailView):
    model = Item
    context_object_name = 'item'
    template_name = 'item_detail.html'

@login_required
def BuyItemView(request, pk=None):
    if pk:
        order = Order.objects.get(id=pk)
        order.paid = True
        order.save()
        return redirect('mainapp:item_list')
    else:
        cart = Cart(request)
        item_id = request.POST.get('ans')
        data = []
        order = Order.objects.create(
            customer = request.user,
            payment_status = item_id
        )
        for item in cart:
            if item_id == "usd":
                price = item['product'].stripe_id_usd
            else:
                price = item['product'].stripe_id_eur

            d = {
                'price': price,
                'quantity': item['quantity'],
            }
            OrderItem.objects.create(
                order = order,
                product = item['product'],
                price = item['price'],
                quantity = item['quantity']
            )
            data.append(d)
        p_link = stripe.PaymentLink.create(
            line_items=data,
            after_completion={
                "type": "redirect",
                "redirect": {"url": "https://iqkids.uz"}
                }
        )
        order.payment_id = p_link.id
        order.payment_link = p_link.url
        order.save()
        cart.clear()
        # session_id = checkout_session = stripe.checkout.Session.create(
        #         line_items=[ data ],
        #         mode='payment',
        #         success_url=M_DOMAIN + '/success',
        #         cancel_url=M_DOMAIN + '/cancel',
        # )

        return JsonResponse({
                'status':'ok',
                'session_id': p_link.url,
        })
