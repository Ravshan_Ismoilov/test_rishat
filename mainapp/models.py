from django.db import models
from decimal import Decimal
import stripe

STATUS_CHOICES = (
		('usd', 'USD'),
		('eur', 'EUR'),
	)
# Create your models here.
class Item(models.Model):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    stripe_id = models.CharField(max_length=255, verbose_name="Stripe id", blank=True, null=True)
    stripe_id_usd = models.CharField(max_length=255, verbose_name="Price id in USD", blank=True, null=True)
    stripe_id_eur = models.CharField(max_length=255, verbose_name="Price id in EUR", blank=True, null=True)
    name = models.CharField(max_length=20, verbose_name="Name")
    description = models.TextField(verbose_name="Description", blank=True, null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Price", null=True, blank=True)
    # currency = models.CharField(max_length=10, choices=STATUS_CHOICES, default='usd', verbose_name="Payment type")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Created date")
    updated = models.DateTimeField(auto_now=True, verbose_name="Updated date")
    
    def save(self, *args, **kwargs):
        # if self.stripe_id == "":
        pro = stripe.Product.create(
                name=self.name,
                description=self.description,
                default_price_data={
                    "unit_amount": int(self.price*100),
                    "currency": "usd",
                    },
                expand=["default_price"],
            )
        self.stripe_id = pro.get("id")
        price = pro.get("default_price")
        self.stripe_id_usd = price["id"]
        pro_cur = stripe.Price.create(
                product=self.stripe_id,
                unit_amount=int(self.price * Decimal(0.96) * 100),
                currency="eur",
            )
        self.stripe_id_eur = pro_cur.get("id")
        return super().save(*args, **kwargs)

    #Metadata
    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name