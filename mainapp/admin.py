from django.contrib import admin
from mainapp.models import Item


class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'price', 'stripe_id', 'stripe_id_usd', 'stripe_id_eur', 'created', 'updated')
    readonly_fields = ('stripe_id', 'stripe_id_usd', 'stripe_id_eur', 'created', 'updated')

admin.site.register(Item, ItemAdmin)

