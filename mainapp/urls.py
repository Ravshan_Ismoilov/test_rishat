from django.urls import path
from mainapp.views import ItemListView, ItemDetailView, BuyItemView

app_name = 'mainapp'

urlpatterns = [
    path('item/<int:pk>/', ItemDetailView.as_view(), name='item_detail'),
    path('buy/<int:pk>/', BuyItemView, name='buy_item'),
    path('buy/', BuyItemView, name='buy_item'),
    path('', ItemListView.as_view(), name='item_list'),
]